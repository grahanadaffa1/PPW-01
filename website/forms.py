from django import forms
from .models import time
from .models import webserviceDB

class Formtime(forms.Form):
    nama_kegiatan = forms.CharField(label = "Nama_kegiatan")
    tempat = forms.CharField(label = "Tempat")
    kategori = forms.CharField(label = "Kategori")
    description = forms.CharField(label = "Description")
    tanggal = forms.DateField(widget=forms.DateInput(attrs={'type' : 'date'}))
    waktu = forms.TimeField(widget=forms.TimeInput(attrs={'type' : 'time'}))


class webserviceForm(forms.Form):
	email = forms.CharField(label = "email", required=True, widget=forms.EmailInput(attrs={'class': 'form-control', 'placeholder': 'Enter Your email'}))
	nama = forms.CharField(label = "nama", required=True,
                             widget=forms.TextInput(
                                 attrs={'class': 'form-control', 'placeholder': 'Enter Your name'})
)
	password = forms.CharField(label = "password", required=True, widget=forms.PasswordInput(
                                   attrs={'class': 'form-control', 'placeholder': 'Enter Your Password'})
)
