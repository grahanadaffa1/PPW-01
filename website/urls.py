from django.urls import path
from django.conf.urls import url

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('subscribe/', views.subscribe, name='subscribe'),
    url(r'^ajax/validate_email/$', views.validate_email, name='validate_email'),
    path('getData/', views.getData, name='getData'),
    path('unsubscribe/', views.unsubscribe, name='unsubscribe'),
]

