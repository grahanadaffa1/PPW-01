from django.test import TestCase, Client, LiveServerTestCase
from ajax.views import data, index
from django.urls import resolve
from django.http import HttpRequest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import unittest
import time
# Create your tests here.

class normalTestCase(TestCase):
	def testUrl(self):
		c = Client()
		response = c.get('/ajax/')
		self.assertTrue(response.status_code, 200)

	def testContent(self):
		self.assertIsNotNone(index)
		self.assertIsNotNone(data)

	# def testContain(self):
	# 	request = HttpRequest()
	# 	response = index(request)
	# 	html_response = response.content.decode('utf8')
	# 	self.assertIn('<h1 style="font-family: helvetica; text-align: center; margin-top: 4vw;">Book List</h1>',html_response)

	def testFunc(self):
		found = resolve('/ajax/')
		self.assertEqual(found.func , index)

	def testTemplate(self):
		response = Client().get('/ajax/')
		self.assertTemplateUsed(response, 'main.html')