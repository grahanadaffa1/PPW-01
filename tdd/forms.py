from django import forms
from .models import Question

class FormStatus(forms.Form):
	attrs_status = {
		'class' : 'status-control',
		'size' : 7,
		'placeholder' : 'Whats up?'
	}

	question_text = forms.CharField(label = "Whats up?", required=True, widget=forms.widgets.TextInput(attrs=attrs_status))