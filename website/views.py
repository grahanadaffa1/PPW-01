from django.shortcuts import render, redirect
from .models import time, webserviceDB
from .forms import Formtime , webserviceForm
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
import json
from django.core import serializers

# Create your views here.
from django.http import HttpResponse

def home(request):
    return render(request, 'website/home.html')

def index(request):
    return render(request, 'website/prototypehtml.html')

def gallery(request):
    return render(request, 'website/gallery.html')

def blog(request):
    return render(request, 'website/blog.html')

def guestBook(request):
    return render(request, 'website/guestBook.html')

def login(request):
    return render(request, 'website/login.html')

def resume(request):
    return render(request, 'website/resume.html')

def schedule(request):
    return render(request, 'website/schedule.html')

def display(request):
    times = time.objects.all()
    response = { "times" : times }
    return render(request, 'website/display.html' , response)

def create_time(request):
    form = Formtime(request.POST or None)
    response = {}
    if (request.method == "POST"):
        if (form.is_valid()):
            nama_kegiatan = request.POST.get("nama_kegiatan")
            tempat = request.POST.get("tempat")
            kategori = request.POST.get("kategori")
            description = request.POST.get("description")
            tanggal = request.POST.get("tanggal")
            waktu = request.POST.get("waktu")
            time.objects.create(nama_kegiatan=nama_kegiatan, tempat=tempat, kategori=kategori, description=description, tanggal=tanggal, waktu=waktu,)
            return redirect ('display')
        else:
            return render(request, 'website/create_time.html', response)
    else:
        response['form'] = form
        return render(request, 'website/create_time.html', response)

def delete(request):
    time.objects.all().delete()
    return redirect('display')

def webservice(request):
	form = webserviceForm(request.POST or None)
	response = {}
	if (request.method == "POST"):
		if(form.is_valid()):
			email = request.POST.get("email")
			nama = request.POST.get("nama")
			password = request.POST.get("password")
			webserviceDB.objects.create(email=email, nama=nama, password=password,)
			return JsonResponse({"Message":"Berhasil"})
		else:
			return JsonResponse({"Message":"TIdak Berhasil"})
	else:
		response['form'] = form
		return render(request, 'website/webservice.html', response)

def subscribe(request):
    response = {}
    if request.method == 'POST':
        subscriber = webserviceForm(request.POST)
        if subscriber.is_valid():
            response['nama'] = request.POST['nama']
            response['email'] = request.POST['email']
            response['password'] = request.POST['password']
            subscriber_database = webserviceDB(nama=request.POST['nama'], email=request.POST['email'], password=request.POST['password'])
            subscriber_database.save()
            # response['message'] = 'Data berhasil disimpan'
            # response['success'] = True
            # json_data = json.dumps(response)
            # return HttpResponse(json_data, content_type='application/json')
            return redirect('subscribe')
        else:
            subscriber.clean()
            # response['message'] = 'Data tidak berhasil disimpan'
            # response['success'] = False
            # json_data = json.dumps(response)
            # return HttpResponse(json_data, content_type='application/json')
            return redirect('subscribe')
    else:
        subscriber = webserviceForm()
        return render (request, 'website/webservice.html', {'form': subscriber})

def validate_email(request):
    email = request.GET.get('email',None)
    data = {
        'is_taken' : webserviceDB.objects.filter(email=email).exists()
    }
    if data['is_taken']:
        data['error_message'] = 'Email tersebut sudah pernah didaftarkan'
    return JsonResponse(data)
    # return redirect('subscribe')

def getData(request):
	all_data = webserviceDB.objects.all().values()
	subscriberhehe = list(all_data)
	return JsonResponse({'all_data' : subscriberhehe})

@csrf_exempt
def unsubscribe(request):
	if request.method == 'POST':
		email = request.POST['email']
		webserviceDB.objects.get(email=email).delete()
		return redirect('subscribe')

