
function disableSubmitButton(){
    if ($('#id_nama').val().length && $('#id_email').val().length && $('#id_password').val().length > 0 &&
        email_is_validated === true) {
        $("input[type=submit]").prop("disabled", false);
    }
    else {
        $("input[type=submit]").prop("disabled", true);
    }
}

function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

function csrfSafeMethod(method) {
    return(/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}
var csrftoken = getCookie('csrftoken');
var email_is_validated = false;
$(document).ready(function () {
    disableSubmitButton();
    $('#id_nama, #id_email, #id_password').on('change',function(){
        disableSubmitButton();
    });
    $.ajaxSetup({
        beforeSend: function (xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader('X-CSRFToken', csrftoken);
            }
        }
    });

	$("#id_email").on({
        'change': function () {
            var email = $(this).closest("form");
            $.ajax({
                url: email.attr("data-validate-email-url"),
                data: email.serialize(),
                dataType: 'json',
                success: function (data) {
                    if (data.is_taken) {
                        alert(data.error_message);
                    }
                }
            })
            .then(function (data) {
                email_is_validated = !(data.is_taken);
            });
        },
    });
});