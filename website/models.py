from django.db import models

# Create your models here.

class time(models.Model):
    nama_kegiatan = models.CharField(max_length=30)
    tempat = models.CharField(max_length=30)
    kategori = models.CharField(max_length=30)
    description = models.CharField(max_length=30)
    tanggal = models.DateField(default = '0001-01-01')
    waktu = models.TimeField(default = '01:01')

class Post(models.Model):
    author = models.ForeignKey(time, on_delete = models.CASCADE)
    content = models.CharField(max_length = 125)

class webserviceDB(models.Model):
	email = models.CharField(max_length=30, unique=True)
	nama = models.CharField(max_length=30)
	password = models.CharField(max_length=30)
