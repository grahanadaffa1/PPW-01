from django.shortcuts import render
from django.http import HttpResponse
from django.http import JsonResponse
from django.contrib.auth import logout
from django.http import HttpResponseRedirect
import requests
import json

# Create your views here.
def data(request, text):
	jsonInput = requests.get('https://www.googleapis.com/books/v1/volumes?q='+text).json()
	if request.user.is_authenticated:
		count = request.session.get('counter',0)
		request.session['counter'] = count
		for key, value in request.session.items():
			print('{} => {}'.format(key, value))
	return JsonResponse(jsonInput)

def index(request):
	if request.user.is_authenticated:
		request.session['user'] = request.user.username
		request.session['email'] = request.user.email
		request.session.get('counter',0)
		print(dict(request.session))
		for key, value in request.session.items():
			print('{} => {}'.format(key,value))
	return render(request,'main.html')

def logoutPage(request):
	request.session.flush()
	logout(request)
	return HttpResponseRedirect('/ajax/')

def tambah(request):
	print(dict(request.session))
	request.session['counter'] = request.session['counter'] + 1
	return HttpResponse(request.session['counter'], content_type = 'application/json')

def kurang(request):
	print(dict(request.session))
	request.session['counter'] = request.session['counter'] - 1
	return HttpResponse(request.session['counter'], content_type = 'application/json')