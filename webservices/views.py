from django.shortcuts import render
from django.http import JsonResponse
# Create your views here.

def fungsi_hello(request):
	name = request.GET['name']
	age = request.GET['age']
	json = JsonResponse( {"name":name,"age":age}, safe=False)
	return json
