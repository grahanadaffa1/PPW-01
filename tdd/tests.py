from django.test import TestCase, Client, LiveServerTestCase
from tdd.views import landing_page, create_status
from django.urls import resolve
from django.http import HttpRequest
from tdd.models import Question
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import unittest
import time

# Create your tests here.

class LandingPageTest(TestCase):
    def testUrl(self):
        c = Client()
        response = c.get('/tdd/')
        self.assertTrue(response.status_code, 200)

    def testContent(self):
        self.assertIsNotNone(landing_page)
        self.assertIsNotNone(create_status)

    def testContain(self):
    	request = HttpRequest()
    	response = landing_page(request)
    	html_response = response.content.decode('utf8')
    	self.assertIn('<title>TDD</title>', html_response)
    	self.assertIn('<h1 class="halo1" id="h1">“Hello, Apa kabar?”</h1>', html_response)

    def testCanPost(self):
    	new_status = Question.objects.create(question_text='hello')
    	allstatus = Question.objects.all().count()
    	self.assertEqual(allstatus,1)

    def testFunc(self):
    	found = resolve('/tdd/')
    	print("hello")
    	print(found)
    	self.assertEqual(found.func , create_status)

    def testTemplate(self):
    	response = Client().get('/tdd/')
    	self.assertTemplateUsed(response, 'tdd/landing-page.html')

class StatusTestCase(unittest.TestCase):
    def setUp(self):
        # self.browser = webdriver.Chrome('/Users/User/Documents/[Kuliah]/[UI]/[PPW]/PPW-1/chromedriver')
        chrome_options = Options()
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(StatusTestCase, self).setUp() 



    def tearDown(self):
        # self.browser.implicity_wait(3)
        # self.browser.quit()
        self.selenium.quit()
        super(StatusTestCase, self).tearDown()


    # def testCanPostAndRetrieve(self):
    #     selenium = self.selenium
    #     selenium.browser.get('http://localhost:8000/tdd/')
    #     question = selenium.find_element_by_id('id_question_text')
    #     submit = selenium.find_element_by_id('submit')
    #     question.send_keys('haalo')
    #     submit.send_keys(Keys.RETURN)
    #     selenium = self.selenium
    #     selenium.get('http://ppw-c-grahanadaffa.herokuapp.com/tdd/')
    #     status = selenium.find_element_by_id('id_question_text')

    #     submit = selenium.find_element_by_id('submit')

    #     status.send_keys('Coba Coba')

    #     submit.send_keys(Keys.RETURN)

    # def testStyleH1(self):
    #     selenium = self.selenium
    #     selenium.get('http://ppw-c-grahanadaffa.herokuapp.com/tdd/')
    #     family = selenium.find_element_by_class_name('halo1').value_of_css_property('font-family')
    #     align = selenium.find_element_by_class_name('halo1').value_of_css_property('text-align')
    #     self.assertEqual(family,'"Times New Roman"')
    #     self.assertEqual(align,'start')

    # def testStyleTD(self):
    #     selenium = self.selenium
    #     selenium.get('http://ppw-c-grahanadaffa.herokuapp.com/tdd/')
    #     align = selenium.find_element_by_tag_name('td').value_of_css_property('text-align')
    #     background = selenium.find_element_by_tag_name('td').value_of_css_property('background-color')
    #     self.assertEqual(align,'right')
    #     self.assertEqual(background,'rgba(179, 217, 255, 1)')

    # def testPositionH1(self):
    #     selenium = self.selenium
    #     selenium.get('http://ppw-c-grahanadaffa.herokuapp.com/tdd/')
    #     family = selenium.find_element_by_class_name('halo1')
    #     location = family.location
    #     size = family.size
    #     # self.assertEqual(location,{'x': 8, 'y': 8})
    #     # self.assertEqual(size, {'height': 46, 'width': 784})

    # def testPositionSubmit(self):
    #     selenium = self.selenium
    #     selenium.get('http://ppw-c-grahanadaffa.herokuapp.com/tdd/')
    #     family = selenium.find_element_by_id('submit')
    #     location = family.location
    #     size = family.size
    #     # self.assertEqual(location,{'x': 8, 'y': 228})
    #     # self.assertEqual(size,{'height': 18, 'width': 52})

    # def testChangeTheme(self):
    #     selenium = self.selenium
    #     selenium.get('http://ppw-c-grahanadaffa.herokuapp.com/tdd/')
    #     time.sleep(2)
    #     h1 = selenium.find_element_by_id('h1')
    #     tombol = selenium.find_element_by_id('ubah_tema')
    #     warna = h1.value_of_css_property('color')
    #     tombol.click()
    #     time.sleep(1)
    #     self.assertEqual(warna, 'rgba(0, 0, 0, 1)')

    
