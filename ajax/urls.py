from django.contrib import admin
from django.urls import path
from . import views

urlpatterns = [
	path('',views.index, name="index_lab9"),
	path('data/<slug:text>',views.data, name='books_data'),
	path('logout/',views.logoutPage, name='logout'),
	path('tambah/',views.tambah, name='tambah'),
	path('kurang/',views.kurang, name='kurang'),
]