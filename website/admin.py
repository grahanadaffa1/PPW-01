from django.contrib import admin

from .models import time
from .models import webserviceDB

admin.site.register(time)
admin.site.register(webserviceDB)
# Register your models here.
