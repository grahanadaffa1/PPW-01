"""mysite URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
from website.views import gallery as gallery
from website.views import blog as blog
from website.views import guestBook as guestBook
from website.views import resume as resume
from website.views import schedule as schedule
from website.views import display as display
from website.views import create_time as create_time
from website.views import delete as delete
from website.views import webservice as webservice
from website.views import login as login
from website.views import home as home
from tdd.views import landing_page as landing_page
from ajax.views import index as index


urlpatterns = [
    path('', include('website.urls')),
    path('admin/', admin.site.urls),
    path('gallery/', gallery, name='gallery'),
    path('blog/', blog, name='blog'),
    path('guestBook/',guestBook, name='guestBook'),
    path('resume/', resume, name='resume'),
    path('schedule/', schedule, name='schedule'),
    path('display/', display, name='display'),
    path('create_time/', create_time, name = 'create_time'),
    path('delete/', delete, name= 'delete'),
    path('webservice/', webservice, name= 'webservice'),

    path('tdd/', include('tdd.urls')),
    path('ajax/', include('ajax.urls')),
    path('webservices/', include('webservices.urls')),
    path('auth/', include('social_django.urls', namespace='social')),
    path('login/', login, name='login'),
    path('home/', home, name='home'),
    
]

